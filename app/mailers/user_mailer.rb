class UserMailer < ApplicationMailer
    default from: "no-responder@vacunassist.com.ar"

    def nuevo_certi
        @usuario = params[:usuario]
        mail(to: @usuario.email, subject: "Nuevo certificado disponible")
    end

    def turno_prox
        @usuario = params[:usuario]
        @fecha = params[:fecha]
        mail(to: @usuario.email, subject: "Turno próximo")
    end
end

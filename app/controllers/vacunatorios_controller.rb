class VacunatoriosController < ApplicationController
  def ver_contacto
    @v = Vacunatorio.all
  end

  def edit
    @vacunatorio = Vacunatorio.find(params[:id])
    render :edit
  end

  def update
    @vac = Vacunatorio.find(params[:id])
    if @vac.update(params.require(:vacunatorio).permit(:email, :telefono))
      flash[:success] = "vacunatorio successfully updated!"
      redirect_to vacunatorios_ver_contacto_path, notice: "El vacunatorio se ha modificado"
    else
      flash.now[:error] = "vacunatorio update failed"
      render :edit
    end
  end

end
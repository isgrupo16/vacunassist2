class AddAplicadaToTurnos < ActiveRecord::Migration[6.1]
  def change
    add_column :turnos, :aplicada, :boolean
  end
end

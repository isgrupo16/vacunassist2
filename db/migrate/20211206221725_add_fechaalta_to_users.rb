class AddFechaaltaToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :fechaalta, :date
  end
end

class AddUseridToTurnos < ActiveRecord::Migration[6.1]
  def change
    add_column :turnos, :user_id, :integer
  end
end

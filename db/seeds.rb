# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

## rails db:drop db:create db:migrate db:seed

u = User.new
u.password = "abc1234"
u.password_confirmation = "abc1234"
u.email = "paciente1@gmail.com"
u.name = "NombreP1"
u.apellido = "ApellidoP1"
u.dni = 12345678
u.nacimiento = Date.today
u.direccion = "530 1234"
u.covid1 = true
u.covid2 = true
u.fiebreamarilla = false
u.gripe = false
u.rol = 1
u.save!

u = User.new
u.password = "abc1234"
u.password_confirmation = "abc1234"
u.email = "admin1@hotmail.com"
u.name = "AdministradorV1"
u.apellido = "ApellidoADM"
u.dni = 12345679
u.nacimiento = Date.today
u.direccion = "530 1235"
u.covid1 = true
u.covid2 = true
u.fiebreamarilla = false
u.gripe = false
u.rol = 3
u.save!

u = User.new
u.password = "abc1234"
u.password_confirmation = "abc1234"
u.email = "enfermero1@hotmail.com"
u.name = "enfermero1"
u.apellido = "ApellidoENF1"
u.dni = 12345680
u.nacimiento = Date.today
u.direccion = "530 1236"
u.covid1 = true
u.covid2 = true
u.fiebreamarilla = false
u.gripe = false
u.rol = 2
u.fechaalta = Date.today
u.save!

u = User.new
u.password = "abc1234"
u.password_confirmation = "abc1234"
u.email = "enfermero2@hotmail.com"
u.name = "enfermero2"
u.apellido = "ApellidoENF2"
u.dni = 12345681
u.nacimiento = Date.today
u.direccion = "530 1237"
u.covid1 = true
u.covid2 = true
u.fiebreamarilla = false
u.gripe = false
u.rol = 2
u.fechaalta = Date.today
u.save!

u = User.new
u.password = "abc1234"
u.password_confirmation = "abc1234"
u.email = "pacienteB@gmail.com"
u.name = "NombrePB"
u.apellido = "ApellidoPB"
u.dni = 12345682
u.nacimiento = Date.today
u.direccion = "530 1238"
u.covid1 = true
u.covid2 = true
u.fiebreamarilla = false
u.gripe = false
u.rol = 1
u.save!

v = Vacunatorio.new
v.nombre = "Vacunatorio1"
v.lugar = "1 y 60"
v.email = "vac1@vacunassist.gob.ar"
v.telefono = "2214637129"
v.save!

v = Vacunatorio.new
v.nombre = "Vacunatorio2"
v.lugar = "3 y 520"
v.email = "vac2@vacunassist.gob.ar"
v.telefono = "2214637128"
v.save!

v = Vacunatorio.new
v.nombre = "Vacunatorio3"
v.lugar = "117 y 42"
v.email = "vac3@vacunassist.gob.ar"
v.telefono = "2214637127"
v.save!

##! es para mostrar error al guardar y corta ejecucion

t = Turno.new
t.fecha = Date.new(2021,11,15)
t.hora = Time.now
t.nombrevacuna = "covid1"
t.idusuario = 1
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 1
t.aplicada = true
t.asistencia = true
t.texto = ""
t.save!

c = Comprobante.new
c.vacuna = "covid1"
c.fecha = Date.new(2021,11,15)
c.hora = Time.now
c.user_id = 1
c.vacunatorio_id = 1
c.save!

t = Turno.new
t.fecha = Date.new(2021,11,16)
t.hora = Time.now
t.nombrevacuna = "covid2"
t.idusuario = 2
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 2
t.aplicada = true
t.asistencia = true
t.texto = ""
t.save!

c = Comprobante.new
c.vacuna = "covid2"
c.fecha = Date.new(2021,11,16)
c.hora = Time.now
c.user_id = 2
c.vacunatorio_id = 1
c.save!

t = Turno.new
t.fecha = Date.new(2021,12,1)
t.hora = Time.now
t.nombrevacuna = "fiebreamarilla"
t.idusuario = 1
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 1
t.aplicada = false
t.asistencia = false
t.texto = ""
t.save!

t = Turno.new
t.fecha = Date.new(2021,12,1)
t.hora = Time.now
t.nombrevacuna = "fiebreamarilla"
t.idusuario = 2
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 2
t.aplicada = false
t.asistencia = true
t.texto = "Esta cursando una patologia que no permite aplicarle la vacuna"
t.save!

t = Turno.new
t.fecha = Date.new(2021,12,3)
t.hora = Time.now
t.nombrevacuna = "gripe"
t.idusuario = 1
t.idvacunatorio = 2
t.vacunatorio_id = 2
t.user_id = 1
t.aplicada = true
t.asistencia = true
t.texto = ""
t.save!

c = Comprobante.new
c.vacuna = "gripe"
c.fecha = Date.new(2021,12,3)
c.hora = Time.now
c.user_id = 1
c.vacunatorio_id = 2
c.save!

t = Turno.new
t.fecha = Date.new(2021,12,6)
t.hora = Time.now
t.nombrevacuna = "gripe"
t.idusuario = 2
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 2
t.aplicada = true
t.asistencia = true
t.texto = ""
t.save!

c = Comprobante.new
c.vacuna = "gripe"
c.fecha = Date.new(2021,12,6)
c.hora = Time.now
c.user_id = 2
c.vacunatorio_id = 1
c.save!

## Turnos para lista turnos diarios

t = Turno.new
t.fecha = Date.new(2021,12,10)
t.hora = Time.now
t.nombrevacuna = "fiebreamarilla"
t.idusuario = 2
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 2
t.aplicada = false
t.asistencia = nil
t.texto = ""
t.save!

t = Turno.new
t.fecha = Date.new(2021,12,10)
t.hora = Time.now
t.nombrevacuna = "gripe"
t.idusuario = 3
t.idvacunatorio = 1
t.vacunatorio_id = 1
t.user_id = 3
t.aplicada = false
t.asistencia = nil
t.texto = ""
t.save!